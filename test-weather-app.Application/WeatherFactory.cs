﻿using test_weather_app.Core.Domain;

namespace test_weather_app.Application
{
    /// <summary>
    /// Статик фабрика погоды
    /// </summary>
    public static class WeatherFactory
    {
        /// <summary>
        /// Сгенерировать прогноз погоды
        /// </summary>
        public static WeatherForecast GetWeatherForecast()
        {
            var temp = Random.Shared.Next(-30, 30);

            return new WeatherForecast
            {
                Date = DateTime.Now,
                TemperatureC = temp,
                Summary = GetSummary(temp),
            };
        }

        private static string GetSummary(int temperatureC)
        {
            if (temperatureC >= -30 && temperatureC < -15)
                return "Очень холодно";
            if (temperatureC >= -15 && temperatureC < 0)
                return "Холодно";
            if (temperatureC >= 0 && temperatureC < 15)
                return "Прохладно";
            if (temperatureC >= 15 && temperatureC < 30)
                return "Тепло";

            return string.Empty;
        }
    }
}
