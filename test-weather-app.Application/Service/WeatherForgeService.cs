﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using test_weather_app.Application.Handler;
using test_weather_app.Core.Domain;

namespace test_weather_app.Application.Service
{
    /// <summary>
    /// Бекграунд сервис создания прогнозов погоды
    /// Каждые 30 секунд создаёт прогноз и передает в хендлер
    /// </summary>
    public class WeatherForgeService : BackgroundService
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;

        /// <summary>
        /// .ctor
        /// </summary>
        public WeatherForgeService(IServiceScopeFactory serviceScopeFactory)
        {
            _serviceScopeFactory = serviceScopeFactory;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                using (var scope = _serviceScopeFactory.CreateScope())
                {
                    var handler = scope.ServiceProvider.GetRequiredService<IHandler<WeatherForecast>>();
                    var weather = WeatherFactory.GetWeatherForecast();
                    await handler.RecordAsync(weather, stoppingToken);
                }

                await Task.Delay(30000, stoppingToken);
            }
        }
    }
}
