﻿
namespace test_weather_app.Application.Handler
{
    /// <summary>
    /// Интерфейс хендлера отмены запросов
    /// </summary>
    public interface ICancellationHandler
    {
        /// <summary>
        /// Получить токен отмены
        /// </summary>
        /// <returns>Токен отмены</returns>
        CancellationToken GetCancellationToken();

        /// <summary>
        /// Прервать текущий запрос
        /// </summary>
        void Cancel();
    }
}
