﻿using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Unicode;
using test_weather_app.Application.Dto;
using test_weather_app.Core.Abstract;
using test_weather_app.Core.Domain;

namespace test_weather_app.Application.Handler
{
    /// <summary>
    /// Хендлер по работе с прогнозами погоды
    /// </summary>
    public class WeatherHandler : IHandler<WeatherForecast>
    {
        private readonly IRepository<WeatherForecast> _repository;
        private readonly IEventLogger _eventLogger;

        /// <summary>
        /// .ctor
        /// </summary>
        public WeatherHandler(IRepository<WeatherForecast> repository, IEventLogger eventLogger)
        {
            _repository = repository;
            _eventLogger = eventLogger;
        }

        public async Task<Response<IEnumerable<WeatherForecast>>> GetAllAsync(CancellationToken cancellationToken)
        {
            try
            {
                await Task.Delay(3000, cancellationToken);
                var forecasts = await _repository.GetAllAsync(cancellationToken);
                return new Response<IEnumerable<WeatherForecast>> { Success = true, Data = forecasts };
            }
            catch (OperationCanceledException)
            {
                _eventLogger.LogWarning("User cancelled request");
                return new Response<IEnumerable<WeatherForecast>> { Success = false, ErrorMessage = "Запрос был прерван пользователем" };
            }
            catch (Exception ex)
            {
                _eventLogger.LogError(ex.Message, ex);
                return new Response<IEnumerable<WeatherForecast>> { Success = false, ErrorMessage = ex.Message };
            }
        }

        public async Task<Response<WeatherForecast>> GetLatestAsync(CancellationToken cancellationToken)
        {
            try
            {
                await Task.Delay(3000, cancellationToken);
                var forecast = await _repository.GetLatestAsync(cancellationToken);
                return new Response<WeatherForecast> { Success = true, Data = forecast }; 
            }
            catch (OperationCanceledException)
            {
                _eventLogger.LogWarning("User cancelled request");
                return new Response<WeatherForecast> { Success = false, ErrorMessage = "Запрос был прерван пользователем" };
            }
            catch (Exception ex)
            {
                _eventLogger.LogError(ex.Message, ex);
                return new Response<WeatherForecast> { Success = false, ErrorMessage = ex.Message };
            }
        }

        public async Task RecordAsync(WeatherForecast entity, CancellationToken cancellationToken)
        {
            var options = new JsonSerializerOptions {
                Encoder = JavaScriptEncoder.Create(UnicodeRanges.BasicLatin, UnicodeRanges.Cyrillic)
            };
            var serializedEntity = JsonSerializer.Serialize(entity, options);
            _eventLogger.LogInfo($"New weather has arrived: {serializedEntity}");
            await _repository.CreateAsync(entity, cancellationToken);
        }

        public async Task ClearHistory(CancellationToken cancellationToken)
        {
            _eventLogger.LogWarning("Weather records has gone with the wind");
            await _repository.ClearData(cancellationToken);
        }
    }
}
