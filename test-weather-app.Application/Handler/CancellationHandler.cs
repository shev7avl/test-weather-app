﻿namespace test_weather_app.Application.Handler
{
    /// <summary>
    /// Хендлер отмены запросов
    /// </summary>
    public class CancellationHandler : ICancellationHandler
    {
        private CancellationTokenSource _cancellationTokenSource;

        /// <summary>
        /// .ctor
        /// </summary>
        public CancellationHandler()
        {
            _cancellationTokenSource = new CancellationTokenSource();
        }

        public CancellationToken GetCancellationToken() 
        {
            return _cancellationTokenSource.Token;
        }

        public void Cancel()
        {
            _cancellationTokenSource.Cancel();
            _cancellationTokenSource.Dispose();
            _cancellationTokenSource = new CancellationTokenSource();
        }
    }
}
