﻿using test_weather_app.Application.Dto;
using test_weather_app.Core.Abstract;

namespace test_weather_app.Application.Handler
{
    /// <summary>
    /// Интерфейс хендлера по работе с моделями
    /// </summary>
    /// <typeparam name="T">Доменная модель</typeparam>
    public interface IHandler<T> where T : BaseEntity
    {
        /// <summary>
        /// Записать в репозиторий
        /// </summary>
        /// <param name="entity">Модель</param>
        /// <param name="cancellationToken">Токен отмены</param>
        Task RecordAsync(T entity, CancellationToken cancellationToken);

        /// <summary>
        /// Получить последнюю запись
        /// </summary>
        /// <param name="cancellationToken">Токен отмены</param>
        Task<Response<T>> GetLatestAsync(CancellationToken cancellationToken);

        /// <summary>
        /// Получить все записи
        /// </summary>
        /// <param name="cancellationToken">Токен отмены</param>
        Task<Response<IEnumerable<T>>> GetAllAsync(CancellationToken cancellationToken);

        /// <summary>
        /// Очистить данные (если сильно насрёт)
        /// </summary>
        /// <param name="cancellationToken">Токен отмены</param>
        Task ClearHistory(CancellationToken cancellationToken);
    }
}
