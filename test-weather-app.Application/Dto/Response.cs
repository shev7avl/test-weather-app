﻿namespace test_weather_app.Application.Dto
{
    /// <summary>
    /// Модель ответа на запрос от UI
    /// </summary>
    /// <typeparam name="T">Параметр типа</typeparam>
    public class Response<T>
    {
        /// <summary>
        /// Запрос выполнился успешно
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Модель данных
        /// </summary>
        public T? Data { get; set; }
        
        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        public string ErrorMessage { get; set; } = string.Empty;
    }
}
