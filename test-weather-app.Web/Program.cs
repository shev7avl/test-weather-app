using Microsoft.EntityFrameworkCore;
using test_weather_app.Application.Handler;
using test_weather_app.Application.Service;
using test_weather_app.Core.Abstract;
using test_weather_app.Core.Domain;
using test_weather_app.DAL.Context;
using test_weather_app.DAL.Repository;
using test_weather_app.Infrastructure.Logging;

namespace test_weather_app.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddRazorPages();
            builder.Services.AddServerSideBlazor();
            builder.Services.AddSingleton<IEventLogger, SeqLogger>();
            builder.Services.AddDbContext<DatabaseContext>(options => options.UseSqlite(@"Filename=..\test-weather-app.DAL\Data\test-weather-app.DB.sqlite"));
            builder.Services.AddTransient<ICancellationHandler, CancellationHandler>();
            builder.Services.AddScoped(typeof(IRepository<>), typeof(SqliteRepository<>));
            builder.Services.AddScoped(typeof(IHandler<WeatherForecast>), typeof(WeatherHandler));
            builder.Services.AddHostedService<WeatherForgeService>();

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.MapBlazorHub();
            app.MapFallbackToPage("/_Host");

            app.Run();
        }
    }
}