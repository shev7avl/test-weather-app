﻿using Microsoft.EntityFrameworkCore;
using test_weather_app.Core.Domain;

namespace test_weather_app.DAL.Context
{
    public class DatabaseContext : DbContext
    {
        public DbSet<WeatherForecast> Forecasts { get; set; }

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
