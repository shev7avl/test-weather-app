﻿using Microsoft.EntityFrameworkCore;
using test_weather_app.Core.Abstract;
using test_weather_app.DAL.Context;

namespace test_weather_app.DAL.Repository
{
    /// <summary>
    /// Реализация интерфейса репозитория для SQLite
    /// </summary>
    public class SqliteRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly DatabaseContext _context;
        private readonly IEventLogger _eventLogger;

        /// <summary>
        /// .ctor
        /// </summary>
        public SqliteRepository(DatabaseContext context, IEventLogger eventLogger)
        {
            _context = context;
            _context.Database.EnsureCreated();
            _eventLogger = eventLogger;
        }

        public async Task CreateAsync(T entity, CancellationToken cancellationToken)
        {
            try
            {
                await _context.Set<T>().AddAsync(entity, cancellationToken);
                await _context.SaveChangesAsync(cancellationToken);
            }
            catch (Exception ex)
            {
                _eventLogger.LogError(ex.Message, ex);
                throw;
            }
        }

        public async Task<IEnumerable<T>> GetAllAsync(CancellationToken cancellationToken)
        {
            try
            {
                return await _context.Set<T>().ToListAsync();
            }
            catch (Exception ex)
            {
                _eventLogger.LogError(ex.Message, ex);
                throw;
            }
        }

        public async Task<T> GetLatestAsync(CancellationToken cancellationToken)
        {
            try
            {
                return await _context.Set<T>().OrderByDescending(o => o.Id).FirstAsync();
            }
            catch (Exception ex) 
            { 
                _eventLogger.LogError(ex.Message, ex); throw; 
            }
        }

        public async Task ClearData(CancellationToken cancellationToken)
        {
            try
            {
                _context.Set<T>().RemoveRange(_context.Set<T>().ToArray());
                await _context.SaveChangesAsync(cancellationToken);
            }
            catch (Exception ex)
            {
                _eventLogger.LogError(ex.Message, ex); throw;
            }
        }
    }
}
