﻿using test_weather_app.Core.Abstract;

namespace test_weather_app.Core.Domain
{
    /// <summary>
    /// Прогноз погоды
    /// </summary>
    public class WeatherForecast: BaseEntity
    {
        /// <summary>
        /// Время
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Температура в градусах Цельсия
        /// </summary>
        public int TemperatureC { get; set; }

        /// <summary>
        /// Температура в градусах Фаренгейта
        /// </summary>
        public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);

        /// <summary>
        /// Сводка по погоде
        /// </summary>
        public string? Summary { get; set; }
    }
}
