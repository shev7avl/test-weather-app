﻿namespace test_weather_app.Core.Abstract
{
    /// <summary>
    /// Интерфейс логгера
    /// </summary>
    public interface IEventLogger
    {
        void LogInfo(string message);

        void LogWarning(string message);

        void LogError(string message, Exception exception);
    }
}
