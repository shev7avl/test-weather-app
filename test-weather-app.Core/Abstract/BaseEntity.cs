﻿namespace test_weather_app.Core.Abstract
{
    /// <summary>
    /// Общий класс для БД моделей с первичным ключом
    /// </summary>
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}
