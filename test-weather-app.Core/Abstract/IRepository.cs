﻿namespace test_weather_app.Core.Abstract
{
    /// <summary>
    /// Дженерик интерфейс репозитория
    /// </summary>
    /// <typeparam name="T">Параметр типа</typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        /// Получить последнюю запись
        /// </summary>
        /// <param name="cancellationToken">Токен отмены</param>
        Task<T> GetLatestAsync(CancellationToken cancellationToken);

        /// <summary>
        /// Получить все записи
        /// </summary>
        /// <param name="cancellationToken">Токен отмены</param>
        Task<IEnumerable<T>> GetAllAsync(CancellationToken cancellationToken);

        /// <summary>
        /// Положить данные в БД
        /// </summary>
        /// <param name="cancellationToken">Токен отмены</param>
        Task CreateAsync(T entity, CancellationToken cancellationToken);

        /// <summary>
        /// Очистить все записи
        /// </summary>
        /// <param name="cancellation">Токен отмены</param>
        Task ClearData(CancellationToken cancellation);
    }
}
