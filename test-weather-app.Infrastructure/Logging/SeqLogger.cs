﻿using Serilog;
using Serilog.Events;
using test_weather_app.Core.Abstract;
using SeriLogger = Serilog.ILogger;

namespace test_weather_app.Infrastructure.Logging
{
    /// <summary>
    /// Реализация Serilog логгера
    /// </summary>
    public class SeqLogger : IEventLogger
    {
        private readonly SeriLogger _logger;

        /// <summary>
        /// .ctor
        /// </summary>
        public SeqLogger()
        {
            _logger = new LoggerConfiguration()
                .WriteTo.Seq("http://seq:5341")
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                .CreateLogger();
        }

        public void LogError(string message, Exception exception)
        {
            _logger.Error(exception, message);
        }

        public void LogInfo(string message)
        {
            _logger.Information(message);
        }

        public void LogWarning(string message)
        {
            _logger.Warning(message);
        }
    }
}
